<h1 align="center"><a href="https://celebraphone.io">Celebraphone</a></h1>
<p align="center">
    <a href="https://gitlab.com/mattmatters/celebraphone/commits/master"><img alt="pipeline status" src="https://gitlab.com/mattmatters/celebraphone/badges/master/pipeline.svg" /></a>
    <a href="https://celebraphone.io"><img alt="Website Status" src="https://img.shields.io/website-up-down-green-red/https/celebraphone.io.svg?label=WebsiteStatus" /></a>
</p>

Real time P2P video chat service.

This is built on, and split off from the [Facial Recognition Web Assembly Project](https://gitlab.com/mattmatters/opencv-wasm).

## Running locally

You need to be up to date with Elixir, Phoenix, Node, & npm, then run `make serve`.

## Thank You Everyone!

This project was made possible by some of the people below.

<img src="/gitlabhub/gitlab.svg" width="300" /><img src="/gitlabhub/Browserstack-logo.svg" width="300" />
