FACE_REPLACE_URL=http://opencvwasm.s3-website-us-east-1.amazonaws.com/wasm
VENDOR_DIR=./assets/vendor

deps:
	mix deps.get
	npm install --prefix assets
	wget -P $(VENDOR_DIR) $(FACE_REPLACE_URL)/bindings.js
	wget -P $(VENDOR_DIR) $(FACE_REPLACE_URL)/face_replace_high.js
	wget -P $(VENDOR_DIR) $(FACE_REPLACE_URL)/face_replace_high.wasm
	wget -P $(VENDOR_DIR) $(FACE_REPLACE_URL)/face_replace_high.data
	gzip -S .data -d $(VENDOR_DIR)/face_replace_high.data
	mv $(VENDOR_DIR)/face_replace_high $(VENDOR_DIR)/face_replace_high.data

serve:
	mix phx.server

e2e:
	npm run e2e

test:
	mix test
	npm run e2e

clean:
	rm $(VENDOR_DIR)/face_replace*
	rm $(VENDOR_DIR)/replacer*
