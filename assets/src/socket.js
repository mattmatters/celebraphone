// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "assets/js/app.js".

// To use Phoenix channels, the first step is to import Socket
// and connect at the socket path in "lib/web/endpoint.ex":
import { Socket } from "phoenix";
import { USER_ID } from './constants';

let socket = new Socket('/socket', {
  params: {
    user_id: USER_ID,
    token: USER_ID,
    user_name: name,
  }
});

socket.connect();

export default socket;
