import premodule from '../../vendor/premodule.js';
import faceMod from '../../vendor/face_replace_high.js';
import bindings from '../../vendor/bindings.js';
import postWork from '../../vendor/post-work.js';

importScripts(premodule, faceMod, bindings, postWork);
