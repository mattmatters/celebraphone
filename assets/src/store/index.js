import Vue from 'vue';
import Vuex from 'vuex';

// Plugins
import createLogger from 'vuex/dist/logger';
import createWorker from './plugins/worker';
import createPresence from './plugins/presence';

// Constants
import { USER_ID, NOTHING } from '../constants';
import types from './types';

import faceInfo from '../assets/faces';

// Worker
import faceWorker from '../worker/face.worker.js';

// Register vuex with the application
Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

const filter = (mutation, stateBefore, stateAfter) => {
  return mutation.type !== types.WASM_RUNNING;
};

export default new Vuex.Store({
  state: {
    user: {
      edit: true,
      name: '',
      id: '',
    },
    lobby: {
      id: '',
      name: '',
      users: {},
    },
    video: {
      status: NOTHING,
      videoApproved: false,
      srcImageLoaded: false,
      wasmUp: false,
      wasmRunning: false,
    },
    face: faceInfo[Math.floor(Math.random()*faceInfo.length)],
    images: {
      src: null,
      out: null,
      getVideoFrame: null,
      setOutImage: null,
      getSrcImage: null,
      setSrcImage: null,
    },
    stream: {
      canvas: null,
      audio: null,
    },
    worker: null,
    mediaStream: null,
    hasMedia: (navigator.mediaDevices && typeof(navigator.mediaDevices.getUserMedia) == 'function'),
  },
  mutations: {
    setName (state, name) {
      state.user.name = name;
    },
    setLobby (state, id) {
      state.lobby.id = id;
    },
    setOutputCb (state, func) {
      state.images.setOutImage = func;
    },
    toggleSetup (state) {
      state.user.edit = !state.user.edit;
    },
    setVideoStatus (state, status) {
      state.video.status = status;
    },
    setSrcImageCb (state, func) {
      state.images.setSrcImage = func;
    },
    getSrcImageCb (state, func) {
      state.images.getSrcImage = func;
    },
    setSrcStatus(state, status) {
      state.video.srcImageLoaded = status;
    },
    setVideoApproved (state) {
      state.video.videoApproved = true;
    },
    setWorkerUp (state, status) {
      state.video.wasmUp = status;
    },
    setWasmRunning (state, status) {
      state.video.wasmRunning = status;
    },
    setVideoCb (state, videoCb) {
      state.images.getVideoFrame = videoCb;
    },
    setMediaStream (state, stream) {
      state.mediaStream = stream;
    },
    setUsers(state, users) {
      const curKeys = Object.keys(state.lobby.users);

      // Remove old ones
      for (let i = 0; i < curKeys.length; i++) {
        if (!users[curKeys[i]]) {
          delete state.lobby.users[curKeys[i]];
        }
      }


      const keys = Object.keys(users).filter(a => a !== USER_ID);

      for (let i = 0; i < keys.length; i++) {
        if (!state.lobby.users[keys[i]]) {
          this.commit(types.CREATE_USER, keys[i], users[keys[i]]);
        } else {
          state.lobby.users[keys[i]].metas = users[keys[i]].metas;
        }
      }
    },
    createUser (state, key, user) {
      state.lobby.users[key] = {};
      state.lobby.users[key].metas = user.metas;
      state.lobby.users[key].connection = new RTCPeerConnection();
    },
    setAudioStream (state, stream) {
      state.stream.audio = stream;
    },
    setCanvasStream (state, stream) {
      state.stream.canvas = stream;
    },
  },
  actions: {
    getAudioStream (context) {
      navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => {
        context.commit(types.SET_AUDIO_STREAM, stream);
      });
    },
    getSrcImg (context) {
      let image = new Image();
      image.src = context.state.face.url;
      return new Promise((accept, reject) => {
        image.onload = accept;
        image.onerror = reject;
      }).then(accept => {
        context.state.images.setSrcImage(image);
        context.commit(types.SET_SRC_STATUS, true);
      });
    },
  },
  modules: {},
  strict: debug,
  plugins: debug ? [createLogger({filter}), createPresence(), createWorker(new faceWorker())] : [new faceWorker()]
});
