export const NOTHING = 'nothing';
export const VIDEO_INIT = 'video_init';
export const WASM_STARTED = 'webpack_started';
export const FACE_UP = 'face_up';
export const SET_NAME = 'setName';
export const SET_LOBBY = 'setLobby';
export const TOGGLE_SETUP = 'toggleSetup';
export const SET_VIDEO_STATUS = 'setVideoStatus';
export const SET_OUT_IMAGE = 'setOutImage';
export const SET_VIDEO_APPROVED = 'setVideoApproved';
export const SET_WORKER_UP = 'setWorkerUp';
export const SET_VIDEO_CB = 'setVideoCb';
export const SET_OUTPUT_CB = 'setOutputCb';
export const GET_SRC_IMG = 'getSrcImg';
export const SET_SRC_IMAGE_CB = 'setSrcImageCb';
export const GET_SRC_IMAGE_CB = 'getSrcImageCb';
export const GET_USER_MEDIA = 'getUserMedia';
export const SET_MEDIA_STREAM = 'setMediaStream';
export const SET_SRC_STATUS = 'setSrcStatus';
export const WASM_RUNNING = 'setWasmRunning';
export const SET_USERS = 'setUsers';
export const SET_CANVAS_STREAM = 'setCanvasStream';
export const SET_AUDIO_STREAM = 'setAudioStream';
export const GET_AUDIO_STREAM = 'getAudioStream';
export const CREATE_USER = 'createUser';

export default {
  CREATE_USER,
  SET_CANVAS_STREAM,
  SET_AUDIO_STREAM,
  GET_AUDIO_STREAM,
  SET_USERS,
  SET_SRC_STATUS,
  NOTHING,
  VIDEO_INIT,
  WASM_STARTED,
  FACE_UP,
  SET_NAME,
  SET_LOBBY,
  TOGGLE_SETUP,
  SET_VIDEO_STATUS,
  SET_SRC_IMAGE_CB,
  GET_SRC_IMAGE_CB,
  SET_OUT_IMAGE,
  SET_VIDEO_APPROVED,
  SET_WORKER_UP,
  SET_VIDEO_CB,
  GET_SRC_IMG,
  GET_USER_MEDIA,
  SET_MEDIA_STREAM,
  WASM_RUNNING,
  SET_OUTPUT_CB,
};
