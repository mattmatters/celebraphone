import { Socket, Presence } from 'phoenix';
import types from '../types';
import socket from '../../socket';

let presences = {};

export default function createPresencePlugin() {
  return store => {
    let channel = socket.channel("lobby:lobby", {});
    channel.join();

    channel.on("presence_state", state => {
      presences = Presence.syncState(presences, state);
        store.commit(types.SET_USERS, presences);
    });

    channel.on("presence_diff", diff => {
      presences = Presence.syncDiff(presences, diff);
        store.commit(types.SET_USERS, presences);
    });

    store.subscribe(mutation => {
      switch (mutation.type) {
      case types.SET_NAME:
        let { name } = store.state.user;
        channel.push("set_user_name", {user_name: name});
        break;
      }
    });
  };
}
