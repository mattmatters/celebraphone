import types from '../types';

export default function createWorkerPlugin(worker) {
  return store => {
    worker.onmessage = function(e) {
      switch (e.data.msg) {
      case 'bootedUp':
        store.commit(types.SET_VIDEO_STATUS, types.WASM_STARTED);
        store.commit(types.SET_WORKER_UP, true);
        break;
      case 'init':
        store.commit(types.SET_VIDEO_STATUS, types.FACE_UP);
        worker.postMessage({
          msg: 'replace',
          image: store.state.images.getVideoFrame(),
        });
        break;
      case 'replaced':
        worker.postMessage({
          msg: 'replace',
          image: store.state.images.getVideoFrame(),
        });
        store.state.images.setOutImage(e.data.image);
        store.commit(types.WASM_RUNNING, true);
        break;
      }
    };

    store.subscribe(mutation => {
      let { state } = store;
      switch (mutation.type) {
      case types.SET_WORKER_UP:
        if (state.video.videoApproved && state.video.srcImageLoaded) {
          worker.postMessage({
            msg: 'init',
            image: state.images.getSrcImage(),
          });

        }
        break;
      case types.SET_VIDEO_APPROVED:
        if (state.video.wasmUp && state.video.srcImageLoaded) {
          worker.postMessage({
            msg: 'init',
            image: state.images.getSrcImage(),
          });
        }
        break;
      case types.SET_SRC_STATUS:
        if (state.video.wasmUp && state.video.videoApproved && state.images.srcImageLoaded) {
          worker.postMessage({
            msg: 'init',
            image: mutation.payload,
          });
        }
        break;
      }
    });
  };
}
