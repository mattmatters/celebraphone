import brando from './brando.jpg';
import dmx from './dmx.jpg';
import busey from './busey.jpg';
import efron from './efron.jpg';
import foxx from './foxx.jpg';
import harvey from './harvey.jpg';
import murphy from './murphy.png';
import nixon from './nixon.jpg';
import obama from './obama.png';
import savage from './savage.jpg';
import snoopp from './snoopp.jpg';
import spacey from './spacey.jpg';

export default [
  {
    name: 'Marlon Brando',
    url: brando,
  },
  {
    name: 'dmx',
    url: dmx,
  },
  {
    name: 'Gary Busey',
    url: busey,
  },
  {
    name: 'Zac Efron',
    url: efron,
  },
  {
    name: 'Jamie Foxx',
    url: foxx,
  },
  {
    name: 'Steve Harvey',
    url: harvey,
  },
  {
    name: 'Eddie Murphy',
    url: murphy,
  },
  {
    name: 'Richard Nixon',
    url: nixon,
  },
  {
    name: 'Barack Obama',
    url: obama,
  },
  {
    name: 'Fred Savage',
    url: savage,
  },
  {
    name: 'Snoopp Dogg',
    url: snoopp,
  },
  {
    name: 'Kevin Spacey',
    url: spacey,
  }
];
