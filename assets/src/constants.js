export const USER_ID = Math.random().toString(36).substr(2, 9);
export const NOTHING = 'nothing';
export const VIDEO_INIT = 'video_init';
export const WASM_STARTED = 'webpack_started';
export const FACE_UP = 'face_up';

export default {
  USER_ID,
  NOTHING,
  VIDEO_INIT,
  WASM_STARTED,
  FACE_UP,
};
