const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');


// Constants
const SOURCE_DIR = './web/static/src';

// Utility
const isProd = (a, b) => process.env.NODE_ENV === 'production' ? a : b;

// Config
module.exports = {
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, '../priv/static/js'),
    filename: 'bundle.js',
    globalObject: 'this',
  },
  mode: isProd('production', 'development'),
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': 'src',
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: {
          loader: 'vue-loader',
          options: {
            compiler: require('vue-template-compiler'),
            cacheBusting: isProd(true, true), // People claim this might fuck shit in dev
            transformToRequire: {
              video: ['src', 'poster'],
              source: 'src',
              img: 'src',
              image: 'xlink:href'
            }
          },
        }
      },
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: [
            /node_modules/,
            /vendor/,
        ],
      },
      {
        test: /\.worker\.js$/,
        use: [
          'babel-loader',
          {
            loader: 'worker-loader',
            options: {
              publicPath: '/js/',
            },
          },
        ],
      },
      {
        test: /\.(js|wasm|data)$/,
        include: [/vendor/],
        use: {
          loader: 'file-loader',
          options: {
            publicPath: '/js/',
            outputPath: '../priv/static/js/',
          },
        },
      },
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'postcss-loader',
        ],
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        use: {
          loader: 'file-loader',
          options: {
            publicPath: '/js/',
            outputPath: '../priv/static/images/',
            limit: 10000,
          },
        },
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        use: {
          loader: 'url-loader',
          options: {
            publicPath: '/images/',
            limit: 10000,
          },
        },
      },
    ],
  },
  plugins: [
    new VueLoaderPlugin(),
    // new CopyWebpackPlugin([
    //   // { from: './vendor', to: '../priv/static/js'},
    //   // { from: './src/assets/faces', to: '../priv/static/images' },
    // ]),
  ],
  node: {
    // prevent webpack from injecting useless setImmediate polyfill because Vue
    // source contains it (although only uses it if it's native).
    setImmediate: false,
    // prevent webpack from injecting mocks to Node native modules
    // that does not make sense for the client
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty'
  },
  serve: {
    hot: true,
    host: 'localhost',
    logLevel: 'trace',
    logTime: true,
    open: false,
  },
};
