defmodule CelebraphoneWeb.PageController do
  use CelebraphoneWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
